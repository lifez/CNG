include:
  - ci_files/variables.yml
  - ci_files/final_images.yml
  # common if and rules definitions
  - local: .gitlab/ci/rules.gitlab-ci.yml
  # common templates
  - local: .gitlab/ci/common.gitlab-ci.yml
  # dependencies.io job
  - local: .gitlab/ci/deps.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE == "true"'
  # build container images
  - local: .gitlab/ci/images.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE != "true"'
  - template: Security/Dependency-Scanning.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE != "true"'

variables:
  # Ensures that existing file permissions are respected when GitLab Runner clones the project.
  FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR: "true"
  # Format of the auto-deploy tag for auto-deploy builds.
  # https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy.md#auto-deploy-tagging
  AUTO_DEPLOY_TAG_REGEX: '^[0-9]+\.[0-9]+\.[0-9]+\+[a-z0-9]{7,}$'
  WF_AUTO_DEPLOY_TAG_REGEX: '/^[0-9]+\.[0-9]+\.[0-9]+\+[a-z0-9]{7,}$/'
  AUTO_DEPLOY_BRANCH_REGEX: '^[0-9]+-[0-9]+-auto-deploy-[0-9]+$'
  # Suffix to the cache key of gitlab-rails-* where Gem vendor bundles are stored. Enables easy cache busting.
  RAILS_CACHE_SUFFIX: "20230622" # quoted string, so `gitlab-runner exec ...` functions
  DNF_OPTS: '--disableplugin=subscription-manager'
  DOCKER_VERSION: 26.0.0
  # Version for all instances of `gitlab-omnibus-builder/distribution_ci_tools`
  CI_TOOLS_VERSION: "5.8.0"
  COSIGN_YES: "true"
  # ARCH_LIST: amd64,arm64
  ARCH_LIST: amd64
  DISABLE_BUILDX_CLUSTER: "false"
  # Buildx cluster connection settings should be set at project level:
  BUILDX_CLUSTER_NAME: ""
  BUILDX_GCLOUD_PROJECT: ""
  BUILDX_CLUSTER_ZONE: ""
  BUILDX_SA_JSON: ""

stages:
  - test
  - prepare
  - prepare:phase-one
  - prepare:phase-two
  - prepare:phase-three
  - prepare:phase-four
  - phase-zero
  - phase-one
  - phase-two
  - phase-three
  - phase-four
  - phase-five
  - phase-six
  - final-list
  - sync
  - release
  - container-scanning
  - report
  - cleanup

workflow:
  name: $PIPELINE_TYPE
  rules:
    # Avoid duplicate pipeline when an MR is open
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"'
      when: never
    # No pipeline on auto-deploy branches as a tag will definitely follow
    - if: '$CI_COMMIT_BRANCH =~ /^[0-9]+-[0-9]+-auto-deploy-[0-9]+$/'
      when: never
    # Support Merge Request pipelines - targetting stable branches
    - if: '($CI_PIPELINE_SOURCE == "merge_request_event" || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME) && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /-stable$/'
    # Support Merge Request pipelines - targetting non-stable branches.
    - if: '($CI_PIPELINE_SOURCE == "merge_request_event" || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME) && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME !~ /-stable$/'
      variables:
        RUBY_VERSION: $NEXT_RUBY_VERSION
    # Run regular pipelines on pushes to regular branches that does not have an MR open
    - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH !~ /-stable$/'
      variables:
        RUBY_VERSION: $NEXT_RUBY_VERSION
    # Run regular pipelines on pushes to stable branches
    - if: '$CI_COMMIT_BRANCH =~ /-stable$/'
    # No pipeline on tag pushes to com. We build them in dev and sync to com
    - if: '$CI_PROJECT_PATH == "gitlab-org/build/CNG" && $CI_COMMIT_TAG'
      when: never
    # For auto-deploy tags, use next Ruby version if the variable is set.
    - if: '$CI_COMMIT_TAG =~ $WF_AUTO_DEPLOY_TAG_REGEX && $USE_NEXT_RUBY_VERSION_IN_AUTODEPLOY == "true"'
      variables:
        PIPELINE_TYPE: AUTO_DEPLOY_BUILD_PIPELINE
        RUBY_VERSION: $NEXT_RUBY_VERSION
    # For auto-deploy tags, set PIPELINE_TYPE to be used as pipeline name in
    # workflow:name
    - if: '$CI_COMMIT_TAG =~ $WF_AUTO_DEPLOY_TAG_REGEX'
      variables:
        PIPELINE_TYPE: AUTO_DEPLOY_BUILD_PIPELINE
    # For all other tags on all other mirrors, create a pipeline.
    - if: '$CI_COMMIT_TAG'

issue-bot:
  stage: report
  image: registry.gitlab.com/gitlab-org/distribution/issue-bot:latest
  script: /issue-bot
  variables:
    ISSUE_BOT_LABELS_EXTRA: "group::distribution,type::maintenance,maintenance::pipelines"
  rules:
    # Only run when pipeline fails for the default branch for the primary project on gitlab.com
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_PATH == "gitlab-org/build/CNG"'
      when: on_failure

danger-review:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:danger
  stage: prepare
  cache: {}
  rules:
    # Do not run on tags, default branch, and stable branches
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^[0-9]+-[0-9]+-stable$/'
      when: never
    # Do not run on child pipelines and triggered pipelines
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "parent_pipeline" || $CI_PIPELINE_SOURCE == "trigger"'
      when: never
    # unless above, if API token defined, run Danger
    - if: '$DANGER_GITLAB_API_TOKEN'
  script:
    - danger --fail-on-errors=true
