#!/bin/bash
# Images that are built nightly on default branch
declare -a nightly_builds=(
  gitlab-rails-ee gitlab-rails-ce
  gitlab-webservice-ce gitlab-webservice-ee
  gitlab-sidekiq-ee gitlab-sidekiq-ce
  gitlab-workhorse-ce gitlab-workhorse-ee
  gitaly gitlab-shell
  gitlab-kas
)

# List of all images that are "final" production images
# Loaded fron CHECKOUT/ci_files/final_images.yml
declare -a final_images=( $(ruby -ryaml -e "puts YAML.safe_load(File.read('ci_files/final_images.yml'))['.final_images'].map {|k| k['job']}.join(' ')") )

function _containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

function is_nightly(){
  [ -n "$NIGHTLY" ] && $(_containsElement $CI_JOB_NAME ${nightly_builds[@]})
}

function is_default_branch(){
  [ "$CI_COMMIT_REF_NAME" == "$CI_DEFAULT_BRANCH" ]
}

function is_stable(){
  [[ "$CI_COMMIT_REF_NAME" =~ ^[0-9]+-[0-9]+-stable(-ee)?$ ]]
}

function force_build(){
  [ "${FORCE_IMAGE_BUILDS}" == "true" ]
}

function should_compile_assets() {
  [ "${COMPILE_ASSETS}" == "true" ]
}

function fetch_assets(){
  [ -z "${ASSETS_IMAGE}" ] && return 1
  should_compile_assets && return 0

  if needs_build; then
    while [ -z "$(skopeo inspect docker://${ASSETS_IMAGE})" ]; do
      echo "${ASSETS_IMAGE} not available yet. Sleeping for 30 seconds";
      sleep 30;
    done
  fi
}

function needs_build(){
  force_build || is_nightly || ! _has_multiarch \"$CI_REGISTRY_IMAGE/${CI_JOB_NAME#build:*}:$CONTAINER_VERSION${IMAGE_TAG_EXT}\" 
}

function _has_multiarch(){
  local _image=${1}
  local _image_manifest=$(skopeo inspect --raw docker://"${_image}")
  local _mediaType=$(echo "${_image_manifest}" | jq -r ".mediaType" )

  local _sha_list
  local _arch
  # used as return values
  local _true=0
  local _false=1
  local _has_pre_built_images=${_true}

  local _arch_list 
  IFS=',' read -r -a _arch_list <<< "${ARCH_LIST}"

  if [ -n "${_mediaType}" ]; then
    if [ ${#_arch_list[@]} -eq 1 ]; then
      # There is only one arch we're building. We're probably OK
        echo "NOTICE: only single arch was specified for ${_image}" >&2
        return ${_true}
    elif [ "$_mediaType" = "application/vnd.oci.image.index.v1+json" -o "$_mediaType" = "application/vnd.docker.distribution.manifest.list.v2+json" ]; then
      for _arch in ${_arch_list[@]}
      do
        _sha_list=$(echo "${_image_manifest}" | jq -r --arg arch "$_arch" '.manifests[] | select (.platform.architecture == $arch) | .digest')
        if [ -z "${_sha_list}" ]; then
          _has_pre_built_images=${_false}
        fi
      done
      local multiarch_str
      if [ "${_has_pre_built_images}" -eq ${_true} ]; then
        multiarch_str="true"
      else
        multiarch_str="false"
      fi
      echo "NOTICE: ${_image} has multiarch images: ${multiarch_str}" >&2
      return ${_has_pre_built_images}
    fi
  else
    return ${_false}
  fi
}

function _print_image_sizes(){
  local _image=${1}
  shift
  local _image_manifest=$(skopeo inspect --raw docker://"${_image}")
  local _platform_manifest
  local _mediaType=$(echo "${_image_manifest}" | jq -r ".mediaType")
  echo "Image information for ${_image}:"
  echo "mediaType: ${_mediaType}"

  if [ "$_mediaType" = "application/vnd.oci.image.index.v1+json" -o "$_mediaType" = "application/vnd.docker.distribution.manifest.list.v2+json" ]; then
    for arch in "$@"
    do
      sha_list=$(echo "${_image_manifest}" | jq -r --arg arch "$arch" '.manifests[] | select (.platform.architecture == $arch) | .digest')
      for sha in ${sha_list}
      do
        image_name=${_image%%:*}
        _platform_manifest=$(skopeo inspect --raw "docker://${image_name}@${sha}")
        total_size=$(echo "${_platform_manifest}" | jq -r '[ .layers[].size ] | add')
        echo "Compressed image size for ${arch} ($sha): ${total_size}"
      done
    done
  else
    # collect desired data
    total_size=$(echo "${_image_manifest}" | jq -r '[ .layers[].size ] | add')
    echo "Compressed image size: ${total_size}"
  fi

}

function build_if_needed(){
  pushd $(get_trimmed_job_name)
  if [ -x renderDockerfile ]; then
    ./renderDockerfile
  fi
  popd

  # IMAGE_REFERENCE_TAG is used to write to the artifact file
  # IMAGE_REFERENCE is used to build, push and sign the image
  local IMAGE_REFERENCE_TAG="${CI_JOB_NAME#build:*}:$CONTAINER_VERSION${IMAGE_TAG_EXT}"
  local IMAGE_REFERENCE="$CI_REGISTRY_IMAGE/${IMAGE_REFERENCE_TAG}"

  if needs_build; then
    local _base_dir=${PWD}
    pushd $(get_trimmed_job_name) # enter image directory

    if [ ! -f "Dockerfile${DOCKERFILE_EXT}" ]; then
      echo "Skipping $(get_trimmed_job_name): Dockerfile${DOCKERFILE_EXT} does not exist."
      popd # be sure to reset working directory
      return 0
    fi

    export BUILDING_IMAGE="true"

    DOCKER_ARGS=( "$@" )

    # Bring in shared scripts
    cp -r ../shared/ shared/

    # Skip the build cache if $DISABLE_DOCKER_BUILD_CACHE is set to any value
    if [ -z ${DISABLE_DOCKER_BUILD_CACHE+x} ]; then
      CACHE_IMAGE="$CI_REGISTRY_IMAGE/${CI_JOB_NAME#build:*}:$CI_COMMIT_REF_SLUG${IMAGE_TAG_EXT}"
      if [ ! "${UBI_BUILD_IMAGE}" = "true" ]; then
        if _has_multiarch "${CACHE_IMAGE}"; then
          echo "NOTICE: Using cache image: ${CACHE_IMAGE}"
        else
          CACHE_IMAGE=""
          if is_stable || is_tag; then
            echo "NOTICE: docker cache image unavailable, disabled for tags and stable branches"
            CACHE_IMAGE=""
          else
            echo "NOTICE: docker cache image unavailable, attempting to use '${CI_DEFAULT_BRANCH}'"
            CACHE_IMAGE="$CI_REGISTRY_IMAGE/${CI_JOB_NAME#build:*}:${CI_DEFAULT_BRANCH}${IMAGE_TAG_EXT}"
            if _has_multiarch "${CACHE_IMAGE}"; then
              echo "NOTICE: docker cache image unavailable, disabling"
              CACHE_IMAGE=""
            fi
          fi
        fi
      else
        if ! $(docker pull $CACHE_IMAGE > /dev/null); then
          if is_stable || is_tag ; then
            echo "NOTICE: docker cache image unavailable, disabled for tags and stable branches"
            CACHE_IMAGE=""
          else
            echo "NOTICE: docker cache image unavailable, attempting to use '${CI_DEFAULT_BRANCH}'"
            CACHE_IMAGE="$CI_REGISTRY_IMAGE/${CI_JOB_NAME#build:*}:${CI_DEFAULT_BRANCH}${IMAGE_TAG_EXT}"
            if ! $(docker pull $CACHE_IMAGE >/dev/null); then
              echo "NOTICE: docker cache image unavailable, disabling"
              CACHE_IMAGE=""
            fi
          fi
        fi
      fi

      if [ -n "${CACHE_IMAGE}" ]; then
        echo "NOTICE: docker cache image in use"
        DOCKER_ARGS+=(--cache-from $CACHE_IMAGE)
      fi
    else
      DOCKER_ARGS+=(--no-cache)
    fi

    # Add build image argument for UBI build stage
    if [ "${UBI_BUILD_IMAGE}" = 'true' ]; then
      [ -z "${BUILD_IMAGE}" ] && export BUILD_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-ubi-builder:master-ubi8"
      DOCKER_ARGS+=(--build-arg BUILD_IMAGE="${BUILD_IMAGE}")
    fi

    if [ "${UBI_PIPELINE}" = 'true' ]; then
      DOCKER_ARGS+=(--build-arg DNF_OPTS="${DNF_OPTS:-}")
    fi

    if [ "${FIPS_PIPELINE}" = 'true' ]; then
      DOCKER_ARGS+=(--build-arg FIPS_MODE="${FIPS_MODE}" --build-arg EXTRA_EXPERIMENT_FLAGS="boringcrypto")
    fi

    openshift_labels=()
    openshift_file_name=
    if [ "${FIPS_PIPELINE}" = 'true' ] && [ -f openshift.metadata.fips ]; then
      openshift_file_name=openshift.metadata.fips
    elif [ "${UBI_PIPELINE}" = 'true' ] && [ -f openshift.metadata.ubi8 ]; then
      openshift_file_name=openshift.metadata.ubi8
    else
      openshift_file_name=openshift.metadata
    fi
    if [ -f $openshift_file_name ]; then
      while read -r label; do
        openshift_labels+=(--label "${label}")
      done < $openshift_file_name
    fi

    tracker_labels=()
    if [ -n "${CI_JOB_URL}" ]; then
      echo "Setting up tracking labels"
      tracker_labels+=(--label "build-url=${CI_JOB_URL}")
      tracker_labels+=(--label "build-job=${CI_JOB_NAME}")
      tracker_labels+=(--label "build-pipeline=${CI_PIPELINE_URL}")
    fi

    # Build images
    # Push new image unless it is a UBI build image
    cp Dockerfile${DOCKERFILE_EXT} ${_base_dir}/artifacts
    if [ ! "${UBI_BUILD_IMAGE}" = 'true' ]; then
      ## Non-UBI image
      echo "Not a UBI build image. Building and pushing the image to the registry."
      echo "Using computed CONTAINER_VERSION (${CONTAINER_VERSION}) as the image tag."
      old_IFS=${IFS}
      export IFS=","
      local _arch_list
      read -r -a _arch_list <<< ${ARCH_LIST}
      local _platforms
      for a in ${ARCH_LIST}
      do
        _platforms=${_platforms}${_platforms:+","}"linux/${a}"
      done
      IFS=${old_IFS}
      set -x
      docker buildx build --platform "${_platforms}" \
        --provenance=false \
        --build-arg CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE \
        --metadata-file "/tmp/metadata.json" \
        -t "${IMAGE_REFERENCE}-buildx" "${DOCKER_ARGS[@]}" \
        -f Dockerfile${DOCKERFILE_EXT} \
        ${DOCKER_BUILD_CONTEXT:-.} \
        "${tracker_labels[@]}" \
        "${openshift_labels[@]}" \
        --provenance=false \
        --push
      set +x

      _has_multiarch "${IMAGE_REFERENCE}-buildx" || exit 1
      _print_image_sizes "${IMAGE_REFERENCE}-buildx" ${_arch_list[@]}
      #FIXME Hack that issupposed to force mediaType to proper value, at least during initial transition:
      skopeo copy --all "docker://${IMAGE_REFERENCE}-buildx" "docker://${IMAGE_REFERENCE}"
      # buildx's --metadata-file doesn't seem to produce proper detailed information, we'll have to fetch it post-factum:
      skopeo inspect --raw docker://${IMAGE_REFERENCE} > ${CI_PROJECT_DIR}/artifacts/images/$(get_trimmed_job_name)${IMAGE_TAG_EXT}-metadata.json

      _has_multiarch "${IMAGE_REFERENCE}" || exit 1

      # Output "Final Remote Compressed Image Size: %d"
      _print_image_sizes "${IMAGE_REFERENCE}" ${_arch_list[@]}

    else
      ## UBI build image
      echo "UBI build image. Only building the image, and not pushing to the registry."
      echo "Using computed CONTAINER_VERSION (${CONTAINER_VERSION}) as the image tag."
      set -x
      docker build \
        --build-arg CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE \
        -t "${IMAGE_REFERENCE}" "${DOCKER_ARGS[@]}" \
        -f Dockerfile${DOCKERFILE_EXT} \
        --metadata-file "/tmp/metadata.json" \
        ${DOCKER_BUILD_CONTEXT:-.} \
        "${tracker_labels[@]}" \
        "${openshift_labels[@]}"
      set +x
      # Output "Final Image Size: %d" (gitlab-org/charts/gitlab#1267)
      docker inspect "$IMAGE_REFERENCE" \
        | awk '/"Size": ([0-9]+)[,]?/{ printf "Final Image Size: %d\n", $2 }'

    fi

    popd # exit image directory

  fi

  image_shasum=$(jq -r '."containerimage.digest"' "/tmp/metadata.json")
  echo "Image built - ${IMAGE_REFERENCE}@${image_shasum}"


  # Record image repository and tag unless it is a UBI build image
  if [ ! "${UBI_BUILD_IMAGE}" = 'true' ]; then
    sign_image "${IMAGE_REFERENCE}" "${image_shasum}"
    echo "${IMAGE_REFERENCE_TAG}" > "artifacts/images/${CI_JOB_NAME#build:*}.txt"
  fi
}

function tag_and_push(){
  local edition=$1
  local mirror_image_name=$2
  local source_image="${CI_REGISTRY_IMAGE}/${CI_JOB_NAME#build:*}:${CONTAINER_VERSION}${IMAGE_TAG_EXT}"
  local target_image="${CI_REGISTRY_IMAGE}/${CI_JOB_NAME#build:*}:${edition}"
  local _arch_list
  read -r -a _arch_list <<< ${ARCH_LIST}

  # If mirror image name is defined, then override the target image name.
  if [ -n "${mirror_image_name}" ]; then
    target_image="${CI_REGISTRY_IMAGE}/${mirror_image_name#build:*}:$edition"
  fi

  # Tag and push unless it is a UBI build image
  if [ ! "${UBI_BUILD_IMAGE}" = 'true' -a -f "$(get_trimmed_job_name)/Dockerfile${DOCKERFILE_EXT}" ]; then
    if [ "${UBI_PIPELINE}" != "true" ] && ! _has_multiarch "${source_image}"; then
      echo "Multiarch criteria for source image are not satisfied ${source_image}"
      exit 1
    fi
    verify_image "${source_image}"

    _print_image_sizes "${source_image}" ${_arch_list[@]}
    cosign copy -f "${source_image}" "${target_image}"
    echo "Copied ${source_image} to ${target_image}"
    _print_image_sizes ${target_image} ${_arch_list[@]}
    if [ "${UBI_PIPELINE}" != "true" ] && ! _has_multiarch "${target_image}"; then
      echo "Multiarch criteria for target image are not satisfied ${target_image}"
      exit 1
    fi
    verify_image "${target_image}"

    echo "Copied ${source_image} to ${target_image}"
  fi

}

function sign_image(){
  if [[ "${SKIP_IMAGE_SIGNING}" == "true" ]]; then
    echo "SKIP_IMAGE_SIGNING set. Not signing image."
    return 0
  fi

  local target_image=$1
  #TODO this is very sensitive to notation used for passing in image reference. 
  local image_name=${target_image%:*}

  # Fetch the digest and compute the immutable image digest reference
  local image_digest=$2
  IMAGE_REFERENCE="${image_name}@${image_digest}"
  echo "Signing image digest: ${IMAGE_REFERENCE}"

  # For pipelines running GitLab.com, use GitLab.com OIDC provider for signing.
  # For other GitLab instances as well as local usage of this script, use a
  # prebuilt private key for signing.
  if [[ "${CI_SERVER_HOST}" == "gitlab.com" ]]; then
    cosign sign --recursive ${IMAGE_REFERENCE}
  elif [ -e "${COSIGN_PRIVATE_KEY}" ]; then
    cosign sign --recursive --key "${COSIGN_PRIVATE_KEY}" "${IMAGE_REFERENCE}"
  else
    echo "Key to be used for signing not defined. Not signing the image digest."
  fi
}

function verify_image()
{
  if [[ "${SKIP_IMAGE_VERIFICATION}" == "true" ]]; then
    echo "SKIP_IMAGE_VERIFICATION set. Not verifying image."
    return 0
  fi

  local target_image=$1
  #TODO this is very sensitive to notation used for passing in image reference
  local image_name=${target_image%:*}

  # Fetch the digest and compute the immutable image digest reference
  image_digest=$(skopeo inspect --format='{{.Digest}}' "docker://${target_image}")
  IMAGE_REFERENCE="${image_name}@${image_digest}"

  echo "Verifying image digest: ${IMAGE_REFERENCE}"

  # From the image reference, extract the registry and project
  registry=$(echo "${target_image}" | cut -d '/' -f1)
  image_reference="${target_image#${registry}/}"
  project=${image_reference%/*}

  # Even though the image reference has `cng` in it, the project path might be
  # in uppercase - `CNG`. For example, https://gitlab.com/gitlab-org/build/CNG
  # uses uppercase, but https://gitlab.com/gitlab-community/build/cng uses
  # lowercase. However, `CI_REGISTRY_IMAGE` variable, which we use to create
  # image references, will have only lowercase characters. So, we can't know
  # for sure how the project path was just from the image reference. OIDC
  # verification requires correct case to be used in the certificate identity.
  # So, we attempt to cover the known case which use uppercase. We also provide
  # a variable to help forks of this project to do the same, without having to
  # make code changes.
  if [[ "${project}" == "gitlab-org/build/cng" ]] || [[ "${COSIGN_CAPITALIZE_PROJECT_NAME}" == "true" ]]; then
    project=${project//cng/CNG}
  fi

  # For images in registry.gitlab.com, we use OIDC for verification. For all
  # other images, we use the keypair for verification.
  if [[ "${registry}" == "registry.gitlab.com" ]]; then
    # For tags, the reference is `refs/tags/<tag_name>`. For branches, it is
    # `refs/heads/<branch_name>`
    if [[ -n "${CI_COMMIT_TAG}" ]]; then
      identity_reference="refs/tags/${CI_COMMIT_TAG}"
    else
      # CI_COMMIT_BRANCH exists in regular branch pipelines,
      # CI_MERGE_REQUEST_SOURCE_BRANCH_NAME exists in MR pipelines
      identity_reference="refs/heads/${CI_COMMIT_BRANCH:-$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
    fi
    certificate_identity="https://gitlab.com/${project}//.gitlab-ci.yml@${identity_reference}"
    echo "Verifying image using certificate identity - ${certificate_identity}"

    cosign verify "${IMAGE_REFERENCE}" --certificate-identity "${certificate_identity}" --certificate-oidc-issuer "https://gitlab.com"
  elif [ -e "${COSIGN_PUBLIC_KEY}" ]; then
    cosign verify --key "${COSIGN_PUBLIC_KEY}" "${IMAGE_REFERENCE}"
  else
    echo "Key to be used for verification not defined. Not verifying the image digest."
  fi
}

function get_version(){
  local _multiarch_suffix=""
  #FIXME Hack adding unique identifier for multiarch images to avoid collisions. Needs further refactoring
  if [ ! "${UBI_PIPELINE}" == "true" ]; then
    _multiarch_suffix="-${ARCH_LIST//,/_}"
  fi
  git ls-tree HEAD -- $1 | awk '{ print $3"'"$_multiarch_suffix"'" }'
}

function get_target_version(){
  get_version $(get_trimmed_job_name)
}

function get_trimmed_job_name(){
  trim_edition ${CI_JOB_NAME#build:*}
}

function is_tag(){
  [ -n "${CI_COMMIT_TAG}" ] || [ -n "${GITLAB_TAG}" ]
}

function is_auto_deploy(){
  [[ $CI_COMMIT_BRANCH =~ $AUTO_DEPLOY_BRANCH_REGEX ]] || [[ $CI_COMMIT_TAG =~ $AUTO_DEPLOY_TAG_REGEX ]]
}

function is_regular_tag(){
  is_tag && ! is_auto_deploy
}

# When `push_tags` is called with `${GITLAB_REF_SLUG}${IMAGE_TAG_EXT}` as
# arguments, we will have something like `v15.1.3-ee-ubi8` or
# `v15.1.3-ee-fips`. We need to strip off the `-ee` part from it.
function trim_edition(){
  echo $1 | sed -e "s/-.e\(-ubi8\|-fips\)\?$/\1/"
}

function trim_tag(){
  echo $(trim_edition $1) | sed -e "s/^v//"
}

function is_final_image(){
  [[ ${final_images[*]} =~ ${CI_JOB_NAME#build:*} ]]
}

function push_tags(){
  if [ ! -f "$(get_trimmed_job_name)/Dockerfile${DOCKERFILE_EXT}" ]; then
    echo "Skipping $(get_trimmed_job_name): Dockerfile${DOCKERFILE_EXT} does not exist."
    return 0
  fi

  local mirror_image_name=$2

  # If a version has been specified and we are on master branch or a
  # non-auto-deploy tag, we use the specified version.
  if [ -n "$1" ] && (is_default_branch || is_regular_tag); then
    echo "Pipeline running against default branch or stable tag. Using specified version as the image tag."
    local edition=$1

    # If on a non-auto-deploy tag pipeline, we can trim the `-ee` suffixes.
    if is_regular_tag; then
      edition=$(trim_edition $edition)
    fi

    version_to_tag=$edition
  elif is_regular_tag; then
    echo "Pipeline running against stable tag and no version specified. Using git tag to as the image tag."
    # If no version is specified, but on a non-auto-deploy tag pipeline, we use
    # the trimmed tag.
    trimmed_tag=$(trim_edition $CI_COMMIT_TAG)

    version_to_tag=$trimmed_tag
  elif [ -z "$1" ]; then
    echo "No version specified. Using commit ref slug as the image tag."
    # If no version was specified at all, we use the slug.
    version_to_tag=${CI_COMMIT_REF_SLUG}${IMAGE_TAG_EXT}
  else
    # If a version was specified on any other scenarios - branch builds or
    # auto-deploy tag builds, we ignore it as we don't want to overwrite an
    # existing versioned image. Since we always call `push_tags` without a
    # version before calling it with a version, the image would've been already
    # tagged with commit ref slug so we need not try to do it again.
    echo "Pipeline running against feature branch or auto-deploy tag. Not tagging the image with specified version."
    version_to_tag=""
  fi

  if [ -n "$version_to_tag" ]; then
    tag_and_push $version_to_tag $mirror_image_name

    # Append the newly pushed tags also to the artifact list
    echo "${CI_JOB_NAME#build:*}:${version_to_tag}" >> "artifacts/images/${CI_JOB_NAME#build:*}.txt"

    # if this is a final image, record it separately.
    if is_final_image; then
      echo "${CI_JOB_NAME#build:*}:${version_to_tag}" > "artifacts/final/${CI_JOB_NAME#build:*}.txt"
    fi
  fi
}

copy_assets() {
  if [ "${UBI_BUILD_IMAGE}" = 'true' ]; then
    ASSETS_DIR="artifacts/ubi/${CI_JOB_NAME#build:*}"
    mkdir -p "${ASSETS_DIR}"
    docker create --name assets "${CI_REGISTRY_IMAGE}/${CI_JOB_NAME#build:*}:${CONTAINER_VERSION}${IMAGE_TAG_EXT}"
    docker cp assets:/assets "${ASSETS_DIR}"
    docker rm assets
    echo "==== Assets Summary ===="
    du -hd2 "${ASSETS_DIR}/assets"
    tar -czf "${ASSETS_DIR}.tar.gz" -C "${ASSETS_DIR}/assets" .
    echo $(sha256sum "${ASSETS_DIR}.tar.gz") $(du -h "${ASSETS_DIR}.tar.gz" | awk '{print $1}')
    rm -rf "${ASSETS_DIR}"
    echo "==== Cleanup UBI artifacts"
    du -hd1 --all artifacts/ubi/*.tar.gz
    for tarball in artifacts/ubi/*.tar.gz ; do
      if [ "${tarball}" != "${ASSETS_DIR}.tar.gz" ]; then
        rm -f "${tarball}"
      fi
    done
  fi
}

use_assets() {
  echo "=== use_assets"
  if [ "${UBI_PIPELINE}" = 'true' -a -f "artifacts/ubi/${CI_JOB_NAME#build:*}.tar.gz" ]; then
    echo "use_assets: triggered on 'artifacts/ubi/${CI_JOB_NAME#build:*}.tar.gz'"
    target="${CI_JOB_NAME#build:*}"
    cp -R "artifacts/ubi/${target}.tar.gz" "${target%*-ee}/${target}.tar.gz"
  fi
}

import_assets() {
  if [ "${UBI_PIPELINE}" = 'true' ]; then
    cp $@ $(get_trimmed_job_name)/
    mock_tags_from_assets
  fi
}

# mock_tags_from_assets
# To support UBI having assets versus artifact containers, we checksum
# the assets tarballs, and use these as the "container_version" content.
mock_tags_from_assets() {
  if [ "${UBI_PIPELINE}" = 'true' ]; then
    trimmed_job_name=$(get_trimmed_job_name)
    assets="${trimmed_job_name}/*.tar.gz"
    shopt -s nullglob
    for asset in $assets; do
      container=$(basename $asset)
      false_tag="artifacts/container_versions/${container%.tar.gz}_tag.txt"
      # Write the sha256sum of the tarball as tag. Use cut to drop the file
      # name from sha256sum's output
      sha256sum $asset | cut -d " " -f 1 > "${false_tag}"
    done
    shopt -u nullglob
  fi
}

# cleanup_mock_tags
# Clean the macked container versions tag files, so they do not pollute
# artifacts that will be used by later jobs.
cleanup_mock_tags() {
  if [ "${UBI_PIPELINE}" = 'true' ] || [ "${FIPS_PIPELINE}" = 'true' ]; then
    echo "==== Cleaning up mocked tags from UBI import_assets"
    shopt -s nullglob
    assets="${trimmed_job_name}/*.tar.gz"
    for asset in $assets; do
      container=$(basename $asset)
      false_tag="artifacts/container_versions/${container%.tar.gz}_tag.txt"
      echo "- Cleaning up mocked tag: '${container}"
      rm "${false_tag}"
    done
    shopt -u nullglob
  fi
}

## record_stable_image
# pull a base image at a tag, record the tag's digest into container_versions
record_stable_image() {
  image=$1
  name=$(image_root_name ${image})
  image_digest=$(skopeo inspect docker://${image} | jq -r '.Digest')
  echo -n "${image_digest}" > "artifacts/container_versions/${name}.txt"
}

## image_root_name
# return the "basename" of an image
# - docker.io/library/alpine:3.15 => alpine
# - docker.io/library/debian:bookworm-slim => debian
image_root_name() {
  IMAGE=$1
  IMAGE=${IMAGE##*/} # remove all leading slashes
  IMAGE=${IMAGE%%:*} # remove longest from end, with :
  IMAGE=${IMAGE%%@*} # remove longest from end, with @
  echo -n $IMAGE
}

## populate_stable_image_vars
# export the various environment variables surrounding stable-ized distribtion images
# If distributions have entries in `container_verions`, export those for use by CI
# and/or scripting
populate_stable_image_vars() {
  # update DEBIAN_IMAGE to full origin & digest
  if [ -f artifacts/container_versions/debian.txt ]; then
    export DEBIAN_DIGEST=$(cat artifacts/container_versions/debian.txt) ;
    export DEBIAN_IMAGE="${DEPENDENCY_PROXY}${DEBIAN_IMAGE}@${DEBIAN_DIGEST}" ;
    export DEBIAN_BUILD_ARGS="--build-arg DEBIAN_IMAGE=${DEBIAN_IMAGE}"
    echo "DEBIAN_BUILD_ARGS: ${DEBIAN_BUILD_ARGS}"
  fi
  # update DEBIAN_IMAGE to full origin & digest
  if [ -f artifacts/container_versions/ubi-minimal.txt ]; then
    export UBI_DIGEST=$(cat artifacts/container_versions/ubi-minimal.txt) ;
    export UBI_IMAGE="${UBI_IMAGE}@${UBI_DIGEST}" ;
    export UBI_BUILD_ARGS="--build-arg UBI_IMAGE=${UBI_IMAGE}"
    echo "UBI_BUILD_ARGS: ${UBI_BUILD_ARGS}"
  fi
  # update ALPINE_IMAGE to full origin & digest
  if [ -f artifacts/container_versions/alpine.txt ]; then
    export ALPINE_DIGEST=$(cat artifacts/container_versions/alpine.txt) ;
    export ALPINE_IMAGE="${DEPENDENCY_PROXY}${ALPINE_IMAGE}@${ALPINE_DIGEST}" ;
    export ALPINE_BUILD_ARGS="--build-arg ALPINE_IMAGE=${ALPINE_IMAGE}"
    echo "ALPINE_BUILD_ARGS: ${ALPINE_BUILD_ARGS}"
  fi
}

## list_artifacts
# helper function to list any/all contents of incoming/outgoing artifacts
# input: subdirectory to `artifacts` on which to focus
list_artifacts() {
    subdirectory=$1
    directory="artifacts"
    if [ -d "${directory}/${subdirectory}" ]; then
      directory="${directory}/${subdirectory}"
    fi
    echo "==== Artifacts Summary ===="
    du -hd2 --all "${directory}"
    echo "==========================="
}


