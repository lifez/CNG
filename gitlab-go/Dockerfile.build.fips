ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9

FROM ${UBI_IMAGE}

ARG GO_VERSION=1.21.9
ARG GO_FIPS_TAG=go1.21.9-1-openssl-fips

RUN INSTALL_PKGS="openssl-devel glibc-devel gcc git tar" &&  \
    microdnf update -y && \
    microdnf install -y --setopt=tsflags=nodocs $INSTALL_PKGS && \
    microdnf clean all -y

RUN mkdir -p /tmp/golang && \
    curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz" \
    | tar -xzC /tmp/golang

ENV PATH="$PATH:/tmp/golang/go/bin"

RUN git clone \
    https://github.com/golang-fips/go.git \
    --branch "${GO_FIPS_TAG}" \
    --single-branch \
    --depth 1 \
    /tmp/golang-fips

RUN cd /tmp/golang-fips && \
    chmod +x scripts/* && \
    git config --global user.email "builder@example.com" && \
    git config --global user.name "Builder" && \
    ./scripts/full-initialize-repo.sh && \
    pushd go/src && \
    GOROOT_FINAL=/usr/local/go CGO_ENABLED=1 ./make.bash && \
    popd && \
    mkdir -p /assets/usr/local && \
    mv go /assets/usr/local/

RUN rm -rf \
        /assets/usr/local/go/pkg/*/cmd \
        /assets/usr/local/go/pkg/bootstrap \
        /assets/usr/local/go/pkg/obj \
        /assets/usr/local/go/pkg/tool/*/api \
        /assets/usr/local/go/pkg/tool/*/go_bootstrap \
        /assets/usr/local/go/src/cmd/dist/dist \
        /assets/usr/local/go/.git*
